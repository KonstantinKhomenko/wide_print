$(document).ready(function(){

	
	/*     loader    */

	$(window).load(function() {

		$(".loader_inner").fadeOut();
		$(".loader").delay(200).fadeOut("slow");
		$("header").css("opacity", 1);
		$("h1").addClass("h1_animat");
	}); // load





	// menu button


	$(".toggle-button").click(function(){

		$(".menu").toggleClass("active-menu");
	});

	$(".toggle-button").click(function() {

	  $(".sandwich").toggleClass("active");

	});

	$(".menu li").click(function(){

		$(".menu").removeClass("active-menu");
		$(".sandwich").removeClass("active");
	});



	/*  menu links  */


	$('.to-why').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-we').offset().top }, 1000);
	}); 

	$('.to-surface').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-surface').offset().top }, 1000);
	}); 

	$('.to-after-print').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-after-print').offset().top }, 1000);
	}); 

	$('.to-product').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-product').offset().top }, 1000);
	}); 

	$('.to-trust').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-trust').offset().top }, 1000);
	}); 

	$('.to-contact').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('footer').offset().top }, 1000);
	}); 



	/*   модальное окно на главном экране   */	


	$(".modal-click").click(function(){
		$(".overlay").addClass("active-overlay");

	});

	$(".modal__close").click(function(){
		$(".overlay").removeClass("active-overlay");
	});




	/*  изменяю сетку бутстрапа на разрешении меньше 768рх   */



	var $window = $(window);

	makeGrid();

	$window.resize(function(){
		
		makeGrid();

	}); //resize


function makeGrid(){

		if(($window).width() < 640 ){

		$(".we-item__grid").removeClass("col-xs-4").addClass("col-xs-12");
		
		}else{

		$(".we-item__grid").removeClass("col-xs-12").addClass("col-xs-4");	

		}

		if(($window).width() < 480 ){

		$(".after-print__item").removeClass("col-xs-6").addClass("col-xs-12");

		}else{

		$(".after-print__item").removeClass("col-xs-12").addClass("col-xs-6");

		}


}; //makeGrid





	/*  animation   */

	$("h2").each(function(){

		var animH2 = $(this);

		$(this).waypoint(function() {
			
			animH2.addClass("h2_animat");

		}, {
			offset : "70%"
		});

	});


	$(".we-item img").waypoint(function() {
		
		$(".we-item img").addClass("we-item_animat");

	}, {
		offset : "60%"
	});


	$(".s-surface").waypoint(function() {
		$(".s-surface li").each(function(index) {
			var ths = $(this);
			setInterval(function() {
				ths.addClass("s-surface__animat");
			}, 400*index);
		});
	}, {
		offset : "80%"
	});



	$(".s-after-print").waypoint(function() {
		$(".s-after-print img").each(function(index) {
			var ths = $(this);
			setInterval(function() {
				ths.addClass("s-after-print__animat");
			}, 400*index);
		});
	}, {
		offset : "60%"
	});

	$(".s-trust").waypoint(function() {

		$(".s-trust img").each(function(index) {
			var ths = $(this);
			setInterval(function() {
				ths.addClass("s-trust_animat");
			}, 500*index);
		});

	}, {
		offset : "80%"
	});




}); // ready()


	/*   fixed menu   */

	// $(window).scroll(function(){

	// 	var Menu = $(".menu-wrap");
	// 	var topMenu = $(".menu-wrap").offset().top;
	// 	var scrollWind = $(window).scrollTop();


	// 	if(scrollWind >= topMenu){
	// 		$(".menu-wrap").addClass("fixed-menu");
	// 	}
	// 	else if(scrollWind <= 300){

	// 		$(".menu-wrap").removeClass("fixed-menu");			
	// 	}		

	// }); //scroll


$(window).scroll(function() {
if ($(this).scrollTop() > 170){  
    $(".menu-wrap").addClass("fixed-menu");
  }
  else{
    $(".menu-wrap").removeClass("fixed-menu");		
  }
});






	/*   google map   */


      function initMap() {
        var uluru = {lat: 50.446161, lng: 30.41634399999998};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          label: {
	          color: '#860a0a',
	          text: 'бульвар Вацлава Гавела, 8',
	          fontWeight: 'bold',
          }
        });

      }


$(function(){
  //2. Получить элемент, к которому необходимо добавить маску
  $("#phone").mask("+3 8(099) 999-99-99");
  $("#phone1").mask("+3 8(099) 999-99-99");
});